//
//  News.h
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface News : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *dateDiffString;
@property (nonatomic, copy) NSArray *imageList;
@property (nonatomic, copy) NSArray *newsContent;

+ (instancetype)newsWithDict:(NSDictionary *)dict;

@end
