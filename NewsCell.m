//
//  NewsCell.m
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import "NewsCell.h"
#import <UIImageView+WebCache.h>

@interface NewsCell ()
@property (weak, nonatomic) IBOutlet UIImageView *image3UrlView;
@property (weak, nonatomic) IBOutlet UILabel *titleView;
@property (weak, nonatomic) IBOutlet UILabel *dateDiffString;
@property (nonatomic, assign) NSInteger row;
@end

@implementation NewsCell

- (void)setNews:(News *)news{
    _news = news;
    
    self.titleView.text = news.title;
    self.dateDiffString.text = news.dateDiffString;
    NSURL *url = [NSURL URLWithString:news.imageList[0][@"imageUrl"]];
//    [self.imageView sd_setImageWithURL:url];
    [self.image3UrlView sd_setImageWithURL:url placeholderImage:nil options:SDWebImageProgressiveDownload];
//    [self.imageView sd_setImageWithURL:news.imageList[0][@"image3Url"]];
//    NSLog(@"%@",news);
//    NSLog(@"%zd",self.contentView.frame.size.height);
}

+ (NSString *)getReuseIdentifier:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return @"headNews";
    }
    return @"news";
}

+ (CGFloat)getRowHigh:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return 200;
    }
    return 100;
}

@end
