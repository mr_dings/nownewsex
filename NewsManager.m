//
//  NewsManager.m
//  nowNewsEx
//
//  Created by Suen Yuen on 17/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import "NewsManager.h"
#import "DGONetworkTools.h"
#import "News.h"
@interface NewsManager ()
@property (nonatomic, strong) NSCache *newsCache;
@end

@implementation NewsManager

+ (instancetype)manger{
    static NewsManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [NewsManager new];
    });
    return instance;
}

- (NSCache *)newsCache{
    if(_newsCache == nil){
        _newsCache = [[NSCache alloc] init];
    }
    return _newsCache;
}

- (void)newsWithStringURL:(NSString *)url Success:(void(^)(NSArray *array))successBlock error:(void(^)(NSError *error))errorBlock{
    if([self.newsCache objectForKey:url]){
        NSLog(@"%@",@"form cache");
        successBlock([self.newsCache objectForKey:url]);
        return;
    }
    [[DGONetworkTools shareManager] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSArray *responseObject) {
        NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:10];
        [responseObject enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            News *new = [News newsWithDict:obj];
            [tempArray addObject:new];
        }];
        if(successBlock){
            NSArray *array = tempArray.copy;
            [self.newsCache setObject:array forKey:url];
            successBlock(tempArray.copy);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(errorBlock)
            errorBlock(error);
    }];
}

@end
