//
//  NewsListController.m
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import "NewsListController.h"
#import "News.h"
#import "NewsCell.h"
#import "NewsManager.h"
#import "NewsDetailViewController.h"

@interface NewsListController ()

@property (nonatomic, strong) NSArray *news;

@end

@implementation NewsListController

- (void)setUrl:(NSString *)url{
    if([_url isEqualToString:url])
        return;
    else{
        _url = url;
        self.news = nil;
        [self.tableView reloadData];
    }
    [[NewsManager manger] newsWithStringURL:url Success:^(NSArray *array) {
        self.news = array;
        NSLog(@"%@",@"network");
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)setNews:(NSArray *)news{
    _news = news;
    NSLog(@"%@",@"reload data");
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.url = @"https://d7lz7jwg8uwgn.cloudfront.net/api//editor_choice.json";
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.news.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsCell *cell = [tableView dequeueReusableCellWithIdentifier:[NewsCell getReuseIdentifier:indexPath] forIndexPath:indexPath];
    
    News *new = self.news[indexPath.row];
    cell.news = new;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    News *new = self.news[indexPath.row];
    [self performSegueWithIdentifier:@"news2detail" sender:new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"%zd",indexPath.row);
//    NewsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    return [NewsCell getRowHigh:indexPath];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NewsDetailViewController *ndvc = segue.destinationViewController;
    ndvc.news = sender;
}

@end
