//
//  HomeViewController.m
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import "HomeViewController.h"
#import "NewsListSection.h"
#import "HomeCell.h"
#import "SectionLabel.h"

@interface HomeViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) NSArray *newsSection;
@property (nonatomic, assign) NSInteger currentIndex;
@end

@implementation HomeViewController

- (void)setNewsSection:(NSArray *)newsSection{
    _newsSection = newsSection;
    
    [self loadnewsSection];
    [self.collectionView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [NewsListSection sectionWithSuccess:^(NSArray *array) {
        self.newsSection = array;
//        [array enumerateObjectsUsingBlock:^(NewsListSection *obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            NSLog(@"%@",obj.url);
//        }];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.flowLayout.minimumLineSpacing = 0;
    self.flowLayout.minimumInteritemSpacing = 0;
    NSLog(@"%@",self.flowLayout);
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.pagingEnabled = YES;
    self.collectionView.bounces = NO;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.flowLayout.itemSize = self.collectionView.bounds.size;
}

- (void)loadnewsSection{
    CGFloat margin = 15;
    CGFloat x = 15;
    for (NewsListSection *section in self.newsSection) {
        SectionLabel *label = [SectionLabel sectionLabelWithName:section.name];
        [self.scrollView addSubview:label];
        
        label.frame = CGRectMake(x, 0, label.bounds.size.width, self.scrollView.bounds.size.height);
        x += label.frame.size.width + margin;
    }
    [self.view bringSubviewToFront:self.scrollView];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.contentSize = CGSizeMake(x - margin, 0);
    self.currentIndex = 0;
    SectionLabel *label = self.scrollView.subviews[self.currentIndex];
    label.scale = 1;
}

#pragma mark - collection view delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.newsSection.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HomeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"news" forIndexPath:indexPath];
    
    NewsListSection *section = self.newsSection[indexPath.item];
    cell.section = section;
    cell.backgroundColor = [UIColor redColor];
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat temp = scrollView.contentOffset.x / scrollView.bounds.size.width;
//    NSInteger index = 0;
//    if(temp > self.currentIndex){
//        index = self.currentIndex +1;
//    }else{
//        index = self.currentIndex -1;
//    }
//    if(index < 0 || index == self.newsSection.count)
//        return;
//    SectionLabel *currentLabel = self.scrollView.subviews[self.currentIndex];
//    SectionLabel *nextLabel = self.scrollView.subviews[index];
    SectionLabel *currentLabel = self.scrollView.subviews[self.currentIndex];
    SectionLabel *nextLabel = nil;
    for (NSIndexPath *indexPath in self.collectionView.indexPathsForVisibleItems) {
        if(indexPath.item != self.currentIndex){
            nextLabel = self.scrollView.subviews[indexPath.item];
            break;
        }
    }
    
    if(nextLabel == nil)
        return;
    CGFloat nextScale = fabs(temp - self.currentIndex);
    CGFloat currentScale = 1 - nextScale;
//    NSLog(@"%f %f",nextScale,currentScale);
    currentLabel.scale = currentScale;
    nextLabel.scale = nextScale;
//    NSLog(@"center = %f",nextLabel.center.x);
    
//    CGFloat labelCenter = nextLabel.center.x ;
//    CGFloat scrollCenter = scrollView.bounds.size.width * 0.5;
//    CGFloat maxOffset = self.scrollView.contentSize.width - self.scrollView.bounds.size.width;
//    CGFloat tagetOffset = nextLabel.center.x - scrollCenter;
//    CGFloat currentOffset = self.scrollView.contentOffset.x;
//    NSLog(@"offset = %f maxoffset = %f",tagetOffset, maxOffset);
//    if(tagetOffset < 0)
//        tagetOffset = 0;
//    else if (tagetOffset > maxOffset)
//        tagetOffset = maxOffset;
//    CGFloat endOffset = currentOffset + (tagetOffset - currentOffset) * nextScale;
//    NSLog(@"%f",endOffset);
//    [self.scrollView setContentOffset:CGPointMake(endOffset, 0) animated:YES];
//    NSLog(@"next = %ld, current = %ld",index, self.currentIndex);
//    NSLog(@"%f",scrollView.contentOffset.x);
//    int index = scrollView.contentOffset.x / scrollView.bounds.size.width;
//    NSLog(@"%d",index);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int index = scrollView.contentOffset.x / scrollView.bounds.size.width;
    self.currentIndex = index;
    
    SectionLabel *label = self.scrollView.subviews[self.currentIndex];
    CGFloat offset = label.center.x - self.scrollView.bounds.size.width * 0.5;
    CGFloat maxOffset = self.scrollView.contentSize.width - label.bounds.size.width - self.scrollView.bounds.size.width;
    if(offset <0)
        offset = 0;
    else if(offset > maxOffset)
        offset = maxOffset + label.bounds.size.width;
    [self.scrollView setContentOffset:CGPointMake(offset, 0) animated:YES];
}

@end









