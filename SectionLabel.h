//
//  SectionLabel.h
//  nowNewsEx
//
//  Created by Suen Yuen on 17/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionLabel : UILabel
+ (instancetype)sectionLabelWithName:(NSString *)name;

@property (nonatomic, assign) CGFloat scale;
@property (nonatomic, assign, getter=isHighlight) BOOL highLight;
@end
