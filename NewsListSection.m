//
//  NewsListSection.m
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import "NewsListSection.h"
#import "DGONetworkTools.h"

@implementation NewsListSection

+ (instancetype)sectionWithDict:(NSDictionary *) dict{
    NewsListSection *section = [self new];
    [section setValuesForKeysWithDictionary:dict];
    return section;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}

+ (void)sectionWithSuccess:(void(^)(NSArray *array))successBlock failure:(void(^)(NSError *error))errorBlock{
    [[DGONetworkTools shareManager] GET:@"https://d7lz7jwg8uwgn.cloudfront.net/apps_resource/ios/ios_config_5_0.json" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary *responseObject) {
        NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:10];
        NSArray *array = responseObject[@"newsListSections"];
        [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NewsListSection *section = [self sectionWithDict:obj];
            static int index = 0;
            if(index % 2 == 0)
                section.url = @"https://d3sli7vh0lsda4.cloudfront.net/api/getNewsListv2?category=119&pageNo=1&pageSize=20";
            else
                section.url = @"https://d7lz7jwg8uwgn.cloudfront.net/api//editor_choice.json";
            index ++;
            [tempArray addObject:section];
        }];
        if(successBlock)
            successBlock(tempArray.copy);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(errorBlock)
            errorBlock(error);
    }];
}

@end
