//
//  SectionLabel.m
//  nowNewsEx
//
//  Created by Suen Yuen on 17/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#define kBIGFONT 18
#define kSMALLFONT 14

#import "SectionLabel.h"

@implementation SectionLabel

+ (instancetype)sectionLabelWithName:(NSString *)name{
    SectionLabel *label = [self new];
    label.text = name;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:kBIGFONT];
    [label sizeToFit];
    label.font = [UIFont systemFontOfSize:kSMALLFONT];
    return label;
}

- (void)setScale:(CGFloat)scale{
    CGFloat max = (kBIGFONT *1.0/ kSMALLFONT) - 1;
//    NSLog(@"%f",max * scale + 1);
    self.transform = CGAffineTransformMakeScale(max * scale + 1, max * scale + 1);
}

- (void)setHighLight:(BOOL)highLight{
//    if (highLight) {
//        self.layer 
//    }
}
@end
