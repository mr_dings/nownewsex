//
//  HomeCell.h
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewsListSection;

@interface HomeCell : UICollectionViewCell

@property (nonatomic, weak) NewsListSection *section;

@end
