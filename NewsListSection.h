//
//  NewsListSection.h
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsListSection : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSNumber *categoryId;

+ (instancetype)sectionWithDict:(NSDictionary *) dict;

+ (void)sectionWithSuccess:(void(^)(NSArray *array))successBlock failure:(void(^)(NSError *error))errorBlock;
@end
