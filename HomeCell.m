//
//  HomeCell.m
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import "HomeCell.h"
#import "NewsListController.h"
#import "NewsListSection.h"

@interface HomeCell ()
@property (nonatomic, strong) NewsListController *newsController;
@end

@implementation HomeCell

-(void)setSection:(NewsListSection *)section{
    
    self.newsController.url = section.url;
//    NSLog(@"%@",section.url);
}

-(void)awakeFromNib{
    [super awakeFromNib];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"News" bundle:nil];
    
    self.newsController = [sb instantiateInitialViewController];
    
    [self.contentView addSubview:self.newsController.view];
}

-(void)layoutSubviews{
    [super layoutSubviews];
//    NSLog(@"%@",@"layout subview");
    self.newsController.view.frame = self.bounds;
    
    NSLog(@"%@ %@",NSStringFromCGRect( self.frame), NSStringFromCGRect( self.bounds));
}

@end
