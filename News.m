//
//  News.m
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import "News.h"
#import "DGONetworkTools.h"

@implementation News

+ (instancetype)newsWithDict:(NSDictionary *)dict{
    News *new = [self new];
    [new setValuesForKeysWithDictionary:dict];
    return new;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}
//
//+ (void)newsWithSuccess:(void(^)(NSArray *array))successBlock error:(void(^)(NSError *error))errorBlock{
//    [self newsWithStringURL:@"https://d7lz7jwg8uwgn.cloudfront.net/api//editor_choice.json" Success:successBlock error:errorBlock];
//}
//
//+ (void)newsWithStringURL:(NSString *)url Success:(void(^)(NSArray *array))successBlock error:(void(^)(NSError *error))errorBlock{
//    + (void)newsWithStringURL:(NSString *)url Success:(void(^)(NSArray *array))successBlock error:(void(^)(NSError *error))errorBlock
//    
//    [[DGONetworkTools shareManager] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSArray *responseObject) {
//        NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:10];
//        [responseObject enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            News *new = [self newsWithDict:obj];
//            [tempArray addObject:new];
//        }];
//        if(successBlock){
//            successBlock(tempArray.copy);
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        if(errorBlock)
//            errorBlock(error);
//    }];
//}

- (NSString *)description{
    return [NSString stringWithFormat:@"[title = %@,\ndata = %@,\nimage = %@]", self.title, self.dateDiffString, self.imageList];
}

@end
