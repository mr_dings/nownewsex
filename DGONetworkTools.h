//
//  DGONetworkTools.h
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFHTTPSessionManager.h>

@interface DGONetworkTools : AFHTTPSessionManager

+ (instancetype)shareManager;

@end
