//
//  NewsManager.h
//  nowNewsEx
//
//  Created by Suen Yuen on 17/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsManager : NSObject

+ (instancetype)manger;
- (void)newsWithStringURL:(NSString *)url Success:(void(^)(NSArray *array))successBlock error:(void(^)(NSError *error))errorBlock;
@end
