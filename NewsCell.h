//
//  NewsCell.h
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"

@interface NewsCell : UITableViewCell

@property (nonatomic, weak) News *news;
+ (NSString *)getReuseIdentifier:(NSIndexPath *)indexPath;
+ (CGFloat)getRowHigh:(NSIndexPath *)indexPath;
@end
