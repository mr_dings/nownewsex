//
//  NewsDetailViewController.m
//  nowNewsEx
//
//  Created by Suen Yuen on 17/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import "NewsDetailViewController.h"
#import <UIImageView+WebCache.h>
#import <Masonry.h>

@interface NewsDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *dateDiffStringButton;
@property (weak, nonatomic) IBOutlet UILabel *newsContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation NewsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.backButton.backgroundColor = [UIColor redColor];
    [self.view bringSubviewToFront:self.backButton];
    
    [self.backButton addTarget:self action:@selector(finish) forControlEvents:UIControlEventTouchUpInside];
//    NSLog(@"%@",_imageView);
//    self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width,0);
    NSURL *url = [NSURL URLWithString:self.news.imageList[0][@"imageUrl"]];
//    [self.imageView sd_setImageWithURL:url];
    [self.imageView sd_setImageWithURL:url completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if(error)
            return;
        CGFloat w = self.imageView.bounds.size.width;
        CGFloat imageW = image.size.width;
        CGFloat scale = imageW / w;
        CGFloat h = image.size.height / scale;
        NSLog(@"%@",[self.imageView constraints]);
        [self.imageView removeConstraints:[self.imageView constraints]];
        
        [self.imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(h);
            NSLog(@"%@",@"abcdef");
        }];
    }];
    self.titleLabel.text = self.news.title;
    [self.dateDiffStringButton setTitle:self.news.dateDiffString forState:UIControlStateNormal];
    NSMutableString *mStr = [NSMutableString string];
    
    NSInteger stopIndex = self.news.newsContent.count - 2;
    
    [self.news.newsContent enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(idx == stopIndex){
            *stop = YES;
        }else{
            [mStr appendString:obj[@"value"]];
        }
    }];
    
    NSMutableAttributedString *str=  [[NSMutableAttributedString alloc] initWithData:[mStr.copy dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(0, str.length)];
//    CGRect rect = [self.contentLabel.attributedText boundingRectWithSize:CGSizeMake(WIDTH - 330 *FITWIDTH, 0) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
//    self.contentLabel.frame = CGRectMake(115 *FITWIDTH, 110 *FITWIDTH, WIDTH - 330 *FITWIDTH, rect.size.height);
//    self.contentLabel.attributedText =  str;
    
    self.newsContent.attributedText = str;
//    NSLog(@"%@",self.scrollView);
}

- (void)finish{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
