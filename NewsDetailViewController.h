//
//  NewsDetailViewController.h
//  nowNewsEx
//
//  Created by Suen Yuen on 17/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"

@interface NewsDetailViewController : UIViewController
@property (nonatomic, weak) News *news;
@end
