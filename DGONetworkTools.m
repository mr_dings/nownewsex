//
//  DGONetworkTools.m
//  nowNewsEx
//
//  Created by Suen Yuen on 15/10/2017.
//  Copyright © 2017年 Suen Yuen. All rights reserved.
//

#import "DGONetworkTools.h"

@implementation DGONetworkTools

+ (instancetype)shareManager {
    static DGONetworkTools *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.timeoutIntervalForRequest = 15;
        
        AFSecurityPolicy *security = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        [security setValidatesDomainName:NO];
        security.allowInvalidCertificates = YES;
        
        instance = [[self alloc] initWithSessionConfiguration:config];
//        instance.securityPolicy = security;
    });
    return instance;
}

@end
